#!/usr/bin/python3
# gen-msx.py for ncps-minus-001.  By cxw/Necroposters.
# This file only works in beats.  The bpm are defined in music.frag.inc.
import csv, pdb
from textwrap import indent, dedent

# *** Make sure these match music.frag.inc!
bpm = 135
bps = bpm/60.0
spb = 60.0/bpm

songinfo="""\
//Poland is Not Yet Lost.  Transcribed by cxw from
//https://web.archive.org/web/20170617055023/http://www.mkidn.gov.pl/media/docs/06_dwa_glosy.pdf"""

nsharps = -1    # key, defined by # of sharps or minus number of flats

# Note definitions: name and frequency (Hz)

# Note frequencies
# Frequencies in Hz, equal-tempered, A440.
# From http://www.phy.mtu.edu/~suits/notefreqs.html
r=('REST',0.0)  # name, freq
if nsharps==6: # six sharps - F# maj (F#, G#, A#, B, C#, D#, F (=E#))
    c4=('C4',277.18)    # middle C
        # actually C#4, and likewise throughout.
    d4=('D4',311.13)
    e4=('E4',349.23)
    f4=('F4',369.99)
    g4=('G4',415.30)
    a4=('A4',466.16)
    b4=('B4',493.88)
    c5=('C5',554.37)
    d5=('D5',622.25)
    e5=('E5',698.46)
    f5=('F5',739.99)
    g5=('G5',830.61)
    a5=('A5',932.33)
    b5=('B5',987.77)
    c6=('C6',1108.73)
    d6=('D6',1244.51)

elif nsharps==0:        # C maj
    c4=('C4',261.63)    # Middle C
    d4=('D4',293.66)
    e4=('E4',329.63)
    f4=('F4',349.23)
    g4=('G4',392)
    a4=('A4',440)
    b4=('B4',493.88)
    c5=('C5',523.25)
    d5=('D5',587.33)
    e5=('E5',659.25)
    f5=('F5',698.46)
    g5=('G5',783.99)
    a5=('A5',880)
    b5=('B5',987.77)
    c6=('C6',1046.5)
    d6=('D6',1174.66)

elif nsharps==-1:        # F maj or d min
    c4=('C4',261.63)
    d4=('D4',293.66)
    e4=('E4',329.63)
    f4=('F4',349.23)
    g4=('G4',392)
    a4=('A4',440)
    b4=('B4',466.16)    # Bb
    c5=('C5',523.25)
    d5=('D5',587.33)
    e5=('E5',659.25)
    f5=('F5',698.46)
    g5=('G5',783.99)
    a5=('A5',880)
    b5=('B5',987.77)
    c6=('C6',1046.5)
    d6=('D6',1174.66)

else:
    raise RuntimeError('Unknown key signature %d sharps'%nsharps)
#endif nsharps

# Tuple listing all the notes we know about.  These are used to generate
# the GLSL constants for the notes.  r must always be first.
notes=(r, c4, d4, e4, f4, g4, a4, b4, c5, d5, e5, f5, g5, a5, b5, c6, d6);

# Reverse map
pitchnum_by_note = {}
for noteidx, note in enumerate(notes):
    pitchnum_by_note[note] = noteidx

# Convenience constants
e=0.5;      # save typing - Eighth note (half-beat).
de=1.5*e;   # dotted eighth
s=0.25;     # Sixteenth

tiny = s*0.25;

# music: list of ("note", duration in beats[, True]).
# If the True is specified, this is a boundary between parts.  The first
# entry is always assumed to be a boundary.

# A useful data-entry command for vim:
# 'a,.g!/#/s/\(\w\+\)\s\+\([0-9a-zA-Z.]\+\)/(\1, \2),

beats_per_measure = 3

# In 3/4, two voices.  melody is the upper voice.
# Durations are in beats (1 = quarter note)
melody=[
    # Blank
    (r, 2*bps, True),
    # Logo fadein
    (r, 3*bps, True),
    # Intropart
    (r, 20, True),
    # Logo fadeout
    (r, 2*bps, True),

    # Mainpart
    # Line 1
    (a4, de,True),
    (b4, s),
    (c5, 1),
    (c5, 1),

    (c5, de),
    (a4, s),
    (d5, e),
    (c5, e),
    (b4, e),
    (a4, e-tiny),
    (r,tiny),

    (g4, de),
    (g4, s),
    (c5, 1.5),
    (b4, e),

    (b4, 1),
    (a4, 1),
    (r, 1),

    # Line 2
    (a4, de),
    (b4, s),
    (c5, 1),
    (c5, 1),

    (c5, de),
    (a4, s),
    (d5, e),
    (c5, e),
    (b4, e),
    (a4, e-tiny),
    (r,tiny),

    (g4, de),
    (d4, s),
    (c5, 1.5),
    (e4, e),

    (g4, 1),
    (f4, 1),
    (r, 1),

    # Line 3
    (g4, 1, True),  # greetz
    (g4, 1.5),
    (b4, e),

    (b4, e),
    (a4, e),
    (a4, 2-2*tiny),
    (r,2*tiny),

    (b4, de),
    (b4, s),
    (b4, 1),
    (b4, e),
    (d5, e),

    (d5, 1),
    (c5, 2-2*tiny),
    (r,2*tiny),

    # Line 4
    (a4, e),
    (c4, e),
    (f5, 1.5),
    (e5, e),

    (e5, e),
    (d5, e),
    (d5, 2-tiny),
    (r,tiny),

    (c5, e),
    (c5, e),
    (b4, 1.5),
    (e4, e),

    (g4, 1),
    (f4, 1),
    (r, 1),

    # other parts added below

    ] #melody

# Note: part-beginning markers (True) are ignored here for sync, but are
# used to tack on the repeat at the end.  Keep them lined up with the
# melody.
harmony=[
    # Blank
    (r, 2*bps, True),
    # Logo fadein
    (r, 3*bps, True),
    # Intropart
    (r, 20, True),
    # Logo fadeout
    (r, 2*bps, True),

    # Mainpart
    # Line 1
    (f4, de, True),
    (g4, s),
    (a4, 1),
    (e4, 1),

    (f4, de),
    (f4, s),
    (f4, 1),
    (d4, e),
    (f4, e),

    (f4, de),
    (f4, s),
    (e4, e),
    (f4, e),
    (g4, 1),

    (g4, 1),
    (f4, 1),
    (r, 1),

    # Line 2
    (f4, de),
    (g4, s),
    (a4, 1),
    (e4, 1),

    (f4, de),
    (f4, s),
    (f4, 1),
    (d4, e),
    (f4, e),

    (f4, de),
    (f4, s),
    (e4, e),
    (d4, e),
    (e4, e),
    (c4, e),

    (e4, 1),
    (f4, 1),
    (r, 1),

    # Line 3
    (c4, 1, True),  # greetz
    (e4, e),
    (f4, e),
    (e4, 1),

    (d4, e),
    (c4, e),
    (f4, 1.5),
    (e4, e),

    (d4, de),
    (d4, s),
    (d4, e),
    (f4, e),
    (b4, e),
    (g4, e),

    (b4, e),
    (a4, e),
    (a4, 2),

    # Line 4
    (f4, e),
    (f4, e),
    (a4, e),
    (b4, e),
    (c5, 1),

    (c5, 1),
    (b4, e),
    (a4, e),
    (g4, e),
    (f4, e),

    (f4, e),
    (f4, e),
    (f4, e),
    (e4, e),
    (d4, e),
    (c4, e),

    (e4, 1),
    (f4, 1),
    (r, 1),

    # other parts added below

    ] #harmony

def finalize(dat):

    # Where the repeat will start
    startrepeatrow = len(dat)

    # Find where line 3 begins - currently the sixth True
    dat2 = ( (x + (False,))[0:3] for x in dat )
        # By default, this note isn't the start of a part (False)

    boundary_number = -1
    repeat_start_row = -1
    for (rowidx, (note, duration, is_part_boundary)) in enumerate(dat2):
        if is_part_boundary:
            boundary_number = boundary_number + 1
            if boundary_number == 5:    #0-indexed
                repeat_start_row = rowidx
                break

    # Tack on the repeats of lines 3 and 4
    dat.extend(dat[repeat_start_row:-1])

    # The repeat is not the start of a new part
    dat[startrepeatrow] = (
        dat[startrepeatrow][0],
        dat[startrepeatrow][1],
        False
    )

    # Credits part
    dat.append( (r, 50, True) )

    # Endpart
    endparts=[
        # done
        (r,1000,True),      #start of the endpart
        (r,1,True)          #end of the endpart
    ]

    dat.extend(endparts)
# finalize()

finalize(melody)
finalize(harmony)

###################################
# Utility routines

def id4(s, level=1):
    """ print _s_ with a four-space indent """
    return indent(dedent(s), prefix=(level*4)*' ')
#id4()

###################################
# Emit the frequencies and periods

print("// Tuning, including REST for consistency")

for (name, freq_hz) in notes:
    print("""\
#define F_%s (%.3f)
#define P_%s (%.20f)\
"""%(name, freq_hz, name, 1.0/freq_hz if freq_hz > 0 else 0))

# Map pitch values to freq, period.  Pitch is rest=0, and after that
print("""
// LUT function from integer pitch number to freq and period
vec2 notefreq(in float pitch_num)
{
    vec2 retval = vec2(F_REST, P_REST);
""")

for (idx, (name, _)) in enumerate(notes):
    print("    retval = mix(retval, vec2(F_%s,P_%s), step(%.1f, pitch_num));"%
        (name, name, idx))

print("""
    return retval;
}
""")

###################################

# Preprocess music
melody.append((r,0))     # always a rest at the end
harmony.append((r,0))     # always a rest at the end

part_boundaries=[]

# Print function header
def gen(name, dat, saveboundaries = False, sustain_control = False):

    # TODO make this a binary-search tree
    print(dedent("""\
    vec4 get_song_data_%s(in float beat_in_pattern)
    {
        vec4 retval; //(pitchnum, dynamics, startbeat, endbeat)
            // frequency < 1.0 => no sound
        retval = vec4(0.0);     // by default, no sound"""%name))
    if sustain_control: print("    // ** Dynamics => sustain control")

    # Print song data.  Track timing as we go.
    currbeat = 0.0

    music2 = ( (x + (False,))[0:3] for x in dat )
        # By default, this note isn't the start of a part (False)

    for (note, duration, is_part_boundary) in music2:
        pitchnum = pitchnum_by_note[note]
        # Pitch number, dynamic-level modifier, starting beat, ending beat

        if sustain_control:     # Exponential - True => sustain longer
            dynamic = 0.1 if is_part_boundary else 1.0
        else:
            dynamic = 1.0 if (currbeat % beats_per_measure < 0.01) else 0.9

        thisnote_vec4 = "vec4(%.1f, %.3f, %.3f, %.3f)"%(
                            pitchnum, dynamic, currbeat, currbeat+duration)
        print(id4("retval = mix(retval, %s, step(%.3f, beat_in_pattern));"%
            (thisnote_vec4, currbeat)))

        if saveboundaries and is_part_boundary:
            part_boundaries.append(currbeat)

        currbeat += duration
    # next note

    # Print function footer
    print(dedent("""\
        return retval;
    } //get_song_data_%s()
    """%name))
# gen()

print("""// Score
%s
"""%songinfo)
gen('melody', melody, True)
gen('harmony', harmony)

# Save any timing information.  Using csv in case I need to add items later.
if len(part_boundaries)>0:
    with open('msx-part-boundaries.csv','w', newline='') as fd:
        outfd = csv.writer(fd, quoting=csv.QUOTE_NONNUMERIC)
        for (idx, boundary) in enumerate(part_boundaries):
            outfd.writerow([boundary])  # Save the boundary for gen-gfx
                # These times are in beats

            # Also stash the boundary data in msx.frag, in seconds
            print("#define START_SECS_%d (%.20f)"%(idx, boundary/bps))
        # next part boundary

# vi: set ts=4 sts=4 sw=4 et ai: #
