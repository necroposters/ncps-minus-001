


// bufa.frag

// by cxw/Necroposters.  CC-BY-SA 3.0
// https://bitbucket.org/necroposters/ncps-minus-001
//This file is generated from gfx.frag.in using perlpp.  DO NOT EDIT.
//Use https://github.com/cxw42/perlpp/, branch 'defines'.



//music.frag.inc: Music parameters and other common foo ##########









#define BPM (270.00000000000000000000)
    // beats per minute
#define BPS (4.50000000000000000000)
    // beats per sec = BPM/60 (110 bpm)
#define SPB (0.22222222222222220989)
    // sec per beat, precomputed
#define BPP (2048.0)
    // beats per pattern - make it longer than the song
    // if you don't want repeats

// DEBUG: where in the demo you want to start.
#define START_OFS_SEC 0.00000000000000000000

// Utils //////////////////////////////////////////////////////////////////
float rand(float n){    // http://shadertoy.wikia.com/wiki/Noise
    // added abs - make sure it's 0..1
    return fract(abs(cos(n*89.42))*343.42);
}
//################################################################




// Reminder: OpenGL-style coord system: +X right, +Y up, +Z toward viewer
// CONFIG CONSTANTS STRUCTS GLOBALS ///////////////////
// {{{1
precision highp int;    //play it safe.  Also covers
precision highp float;  //vectors and matrices.



// Round text
#define ARC_CENTER (vec2(0.0, -10.0))
    // Center of the arc in the plane of the text.  World coords, not screen.
#define ARC_DEAD_RADIUS (10.0)
    // Radial dead zone around ARC_CENTER

// Fixed text grid used for do_hey()
#define HEY_X_CELLS (9.0)
#define HEY_Y_CELLS (3.0)
#define HEY_Y_VOFS (0.5)

// How main samples from BUFA

    



// Voxel logo and related
#define VLOGO_ORIGIN (vec3(6.0, 1.0, -2.0))
#define MESSAGE_Z (0.1)

// Bump-mapping
#define TEX_SCALE (0.2)
    // iChannel0 is Lichen.

// Timing &c.
#define LOADING_FREQ (0.2)

#define S_SINE_FREQ (0.02380952380952380952380952380952)
    // 1/42.  sine letter frequency in cycles per X unit.
#define S_SINE_GROUP_FREQ (0.03125)
    // 1/32.  sine group frequency.  If > S_SINE_FREQ, sine gradually
    // shifts right on screen.
#define CYL_RADIUS (15.0)
    // radius of the cylinder for CYL and later parts
#define TUNNEL_ACCEL (17.0)
    // Acceleration during TUNNEL, in x units per sec^2
#define CUBE_FADEIN_TIME (3.0)
    //fadein in seconds

#define TWOSIDED_RATE (0.3)
    // how fast you go back and forth

#define PI (3.1415926535897932384626433832795028841971)
    // from memory :)
#define PI_OVER_2 (1.5707963267948966192313216916398)
#define PI_OVER_4 (0.78539816339744830961566084581988)
#define THREE_PI_OVER_4 (2.3561944901923449288469825374596)
#define TWO_PI (6.283185307179586)
#define ONE_OVER_TWO_PI (0.15915494309644431437107064141535)
    // not from memory :) :)

#define TEE_ZERO (0.001)
#define TEE_ONE (99999999999999999999.0)
    // +Inf => 1.0/0.0 gives "divide by zero during constant folding" error

#define LTR_Z_THICKNESS (1.0)
    // in voxels
#define EPS (0.000001)
    // Arbitrary choice

#define MAX_VOXEL_STEPS (60)
    // Empirical - gives a nice disappearing effect at the edges on my
    // test system
#define MAX_DIST (100.0)

// xport config
#define XP_GAIN_HZ (3.0)

#define XP_SHINE_LOW (6.0)
#define XP_SHINE_HIGH (32.0)
#define XP_SHINE_AIM (24.0)
    // what it settles down to
#define XP_SHINE_HZ (5.5)

// Hack the camera
#define SMALLEST_CAM_FRACTION (.00001)

// Sinusoids across the u axis (horz)

#define XP_H0_PER_U (30.00000000000000000000)
    // cycles per screen width (u coordinate 0..1)
#define XP_H0_PHASE (0.00000000000000000000)
    // initial phase
#define XP_H0_PHASE_PER_SEC (1.36590984938686665906)
    // how much the phase of the sinusoid changes per second

#define XP_H1_PER_U (10.00000000000000000000)
    // cycles per screen width (u coordinate 0..1)
#define XP_H1_PHASE (1.00000000000000000000)
    // initial phase
#define XP_H1_PHASE_PER_SEC (0.10000000000000000555)
    // how much the phase of the sinusoid changes per second

#define XP_H2_PER_U (5.00000000000000000000)
    // cycles per screen width (u coordinate 0..1)
#define XP_H2_PHASE (0.00000000000000000000)
    // initial phase
#define XP_H2_PHASE_PER_SEC (1.00000000000000000000)
    // how much the phase of the sinusoid changes per second


// }}}1

// CHARSET FOR WORLD-COORDINATE LETTERS ///////////////
// {{{1
/*
^ +Y
9    00000
8  1   2   3
7  1   2   3
6  1   2   3
5    44444
4  5       6
3  5       6
2 858      6
1 88877777
0 888
   0 12345 6  --> +X
(lowercase x, dot, and bang are handled separately)
*/

// Character storage
#define NSEGS (9)
vec4 SEG_SHAPES[NSEGS];
    // All polys will be quads in the X-Y plane, Z=0.
    // All quad edges are parallel to the X or Y axis.
    // These quads are encoded in a vec4: (.x,.y) is the LL corner and
    // (.z,.w) is the UR corner (coords (x,y)).

vec4 SEG_VOXELS[NSEGS];
    // Same deal, but voxel offsets, start->last+1

// Grid parameters - 2D
#define GRID_CHARHT (10.0)
#define GRID_CHARWD (6.0)
    // Size of each character
#define GRID_PITCH (7.0)
    //each char takes up this much space.  Margin is added on the right
    //and is GRID_PITCH-GRID_CHARWD wide.
#define GRID_PITCH_RECIP (0.14285714285714285714285714285714)
    // avoid a division
#define GRID_VPITCH (12.0)
    // margin is added on top
#define THICKNESS (1.0)
    // how thick each stroke is

#define GRID_XSHIFT (GRID_PITCH * 0.5)
    // + pitch/0.5 because letters were snapping into
    // existence at the right side of the screen.

// Grid parameters - voxels.  Twice the size.
#define VGRID_CHARHT (GRID_CHARHT*2.0)
#define VGRID_CHARWD (GRID_CHARWD*2.0)
#define VGRID_PITCH (GRID_PITCH*2.0)
#define VGRID_PITCH_RECIP (GRID_PITCH_RECIP*0.5)
#define VGRID_VPITCH (GRID_VPITCH*2.0)

// For upright chars, each char (X,Y) goes from (PITCH*ofs, 0)->(.+WD,HT).

void init_charset()
{
    float halft = THICKNESS*0.5;
    float halfht = GRID_CHARHT * 0.5;

    SEG_SHAPES[0] = vec4(THICKNESS, GRID_CHARHT - THICKNESS, GRID_CHARWD-THICKNESS, GRID_CHARHT);
    SEG_SHAPES[1] = vec4(0.0,                   halfht, THICKNESS,             GRID_CHARHT - halft);
    SEG_SHAPES[2] = vec4(GRID_CHARWD*0.5-halft, halfht, GRID_CHARWD*0.5+halft, GRID_CHARHT - halft);
    SEG_SHAPES[3] = vec4(GRID_CHARWD-THICKNESS, halfht, GRID_CHARWD,           GRID_CHARHT - halft);
    SEG_SHAPES[4] = vec4(THICKNESS, halfht - halft, GRID_CHARWD-THICKNESS, halfht + halft);
    SEG_SHAPES[5] = vec4(0.0,                   halft,    THICKNESS,             halfht );
    SEG_SHAPES[6] = vec4(GRID_CHARWD-THICKNESS, halft,    GRID_CHARWD,           halfht );
    SEG_SHAPES[7] = vec4(THICKNESS, 0, GRID_CHARWD-THICKNESS, THICKNESS);
    SEG_SHAPES[8] = vec4(0.0, 0.0, THICKNESS, THICKNESS); //dot

    // Voxel grid #1 - not currently in use
    //Grid_Origin = vec3(GRID_XSHIFT, 0, 0);
    //Grid_Spacings = vec3(1.0);
    //Grid_Spacings_Inv = vec3(1.0)/Grid_Spacings;

    // TODO rewrite in terms of #defines.
    // Z, W are +1 so can use IsPointInRectXY, which does not include the
    // UR corner in the poly.
    // Size has been doubled, so we can use multiples of 0.5.
    SEG_VOXELS[0] = vec4(1.0, 9.0, 5.0,10.0)*vec4(2.0);
    SEG_VOXELS[1] = vec4(0.0, 5.5, 1.0, 9.5)*vec4(2.0);
    SEG_VOXELS[2] = vec4(3.0, 6.0, 4.0, 9.0)*vec4(2.0);
    SEG_VOXELS[3] = vec4(5.0, 5.5, 6.0, 9.5)*vec4(2.0);
    SEG_VOXELS[4] = vec4(1.0, 5.0, 5.0, 6.0)*vec4(2.0);
    SEG_VOXELS[5] = vec4(0.0, 1.5, 1.0, 5.5)*vec4(2.0);
    SEG_VOXELS[6] = vec4(5.0, 1.5, 6.0, 5.5)*vec4(2.0);
    SEG_VOXELS[7] = vec4(1.0, 1.0, 6.0, 2.0)*vec4(2.0);
    SEG_VOXELS[8] = vec4(0.0, 0.0, 2.0, 2.0)*vec4(2.0);

} //init_charset

// }}}1

// MESSAGE ////////////////////////////////////////////
// No music sync found
// Parts and start times
#define BLANK (0.0)
#define BLANK_START (0.00000000000000000000)
#define LINE1 (1.0)
#define LINE1_START (2.00000000000000000000)
#define LINE2 (2.0)
#define LINE2_START (8.33333333333333214910)
#define LINE3 (3.0)
#define LINE3_START (16.66666666666666429819)
#define LINE4 (4.0)
#define LINE4_START (23.49999999999999644729)
#define LINE5 (5.0)
#define LINE5_START (28.83333333333332859638)
#define LINE6 (6.0)
#define LINE6_START (33.16666666666666429819)
#define NAMEUS (7.0)
#define NAMEUS_START (40.66666666666666429819)
#define GREET (8.0)
#define GREET_START (47.33333333333332859638)
#define CREDZ (9.0)
#define CREDZ_START (58.83333333333332859638)
#define LAST1 (10.0)
#define LAST1_START (69.00000000000000000000)
#define NCPS1 (11.0)
#define NCPS1_START (74.50000000000000000000)
#define YEAR1 (12.0)
#define YEAR1_START (77.50000000000000000000)
#define ENDPART (13.0)
#define ENDPART_START (92.50000000000000000000)

vec4 get_story(in float time)
{   //returns vec4(partnum, charidx_frac, first_charidx, clip_charidx)
    // NOTE: charidx_frac restarts at 0 each part!
    // first_charidx and clip_charidx are with respect to the whole messge.
    // Character indices starting with clip_charidx should not be displayed.
    float partnum, charidx_frac, first_charidx, clip_charidx;
    if(time<2.00000000000000000000) {
        partnum=BLANK;
        charidx_frac=(time-BLANK_START)*0.50000000000000000000;
        first_charidx=0.0;
        clip_charidx=0.0;
    } else

    if(time<8.33333333333333214910) {
        partnum=LINE1;
        charidx_frac=(time-LINE1_START)*6.00000000000000000000;
        first_charidx=1.0;
        clip_charidx=32.0;
    } else

    if(time<16.66666666666666429819) {
        partnum=LINE2;
        charidx_frac=(time-LINE2_START)*6.00000000000000000000;
        first_charidx=39.0;
        clip_charidx=85.0;
    } else

    if(time<23.49999999999999644729) {
        partnum=LINE3;
        charidx_frac=(time-LINE3_START)*6.00000000000000000000;
        first_charidx=89.0;
        clip_charidx=130.0;
    } else

    if(time<28.83333333333332859638) {
        partnum=LINE4;
        charidx_frac=(time-LINE4_START)*6.00000000000000000000;
        first_charidx=130.0;
        clip_charidx=162.0;
    } else

    if(time<33.16666666666666429819) {
        partnum=LINE5;
        charidx_frac=(time-LINE5_START)*6.00000000000000000000;
        first_charidx=162.0;
        clip_charidx=188.0;
    } else

    if(time<40.66666666666666429819) {
        partnum=LINE6;
        charidx_frac=(time-LINE6_START)*6.00000000000000000000;
        first_charidx=188.0;
        clip_charidx=229.0;
    } else

    if(time<47.33333333333332859638) {
        partnum=NAMEUS;
        charidx_frac=(time-NAMEUS_START)*3.00000000000000000000;
        first_charidx=233.0;
        clip_charidx=253.0;
    } else

    if(time<58.83333333333332859638) {
        partnum=GREET;
        charidx_frac=(time-GREET_START)*6.00000000000000000000;
        first_charidx=253.0;
        clip_charidx=318.0;
    } else

    if(time<69.00000000000000000000) {
        partnum=CREDZ;
        charidx_frac=(time-CREDZ_START)*6.00000000000000000000;
        first_charidx=322.0;
        clip_charidx=379.0;
    } else

    if(time<74.50000000000000000000) {
        partnum=LAST1;
        charidx_frac=(time-LAST1_START)*6.00000000000000000000;
        first_charidx=383.0;
        clip_charidx=412.0;
    } else

    if(time<77.50000000000000000000) {
        partnum=NCPS1;
        charidx_frac=(time-NCPS1_START)*3.00000000000000000000;
        first_charidx=416.0;
        clip_charidx=424.0;
    } else

    if(time<92.50000000000000000000) {
        partnum=YEAR1;
        charidx_frac=(time-YEAR1_START)*0.59999999999999997780;
        first_charidx=425.0;
        clip_charidx=433.0;
    } else

    if(time<1092.50000000000000000000) {
        partnum=ENDPART;
        charidx_frac=(time-ENDPART_START)*0.00100000000000000002;
        first_charidx=434.0;
        clip_charidx=434.0;
    } else

    {
        partnum=0.0;
        charidx_frac=0.0;
        first_charidx=0.0;
        clip_charidx=0.0;
    }

    return vec4(partnum,charidx_frac,first_charidx,clip_charidx);
} //get_story

vec4 get_seg_vec4(float vecidx) {
    if(vecidx>=54.0){
        if(vecidx>=81.0){
            if(vecidx>=95.0){
                if(vecidx>=102.0){
                    if(vecidx>=105.0){
                        if(vecidx>=108.0) return vec4(251.0,0.0,0.0,0.0);
                        if(vecidx>=107.0) return vec4(0.0,185.0,235.0,72.0);
                        if(vecidx>=106.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=105.0) return vec4(107.0,163.0,59.0,211.0);
                    }else{
                        if(vecidx>=104.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=103.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=102.0) return vec4(211.0,21.0,248.0,187.0);
                    }
                }else{
                    if(vecidx>=98.0){
                        if(vecidx>=101.0) return vec4(114.0,187.0,3.0,0.0);
                        if(vecidx>=100.0) return vec4(187.0,0.0,27.0,178.0);
                        if(vecidx>=99.0) return vec4(11.0,0.0,178.0,114.0);
                        if(vecidx>=98.0) return vec4(27.0,26.0,0.0,27.0);
                    }else{
                        if(vecidx>=97.0) return vec4(187.0,187.0,0.0,218.0);
                        if(vecidx>=96.0) return vec4(0.0,0.0,0.0,211.0);
                        if(vecidx>=95.0) return vec4(0.0,0.0,0.0,0.0);
                    }
                }
            }else{
                if(vecidx>=88.0){
                    if(vecidx>=91.0){
                        if(vecidx>=94.0) return vec4(26.0,219.0,114.0,0.0);
                        if(vecidx>=93.0) return vec4(211.0,187.0,11.0,27.0);
                        if(vecidx>=92.0) return vec4(3.0,178.0,0.0,59.0);
                        if(vecidx>=91.0) return vec4(26.0,59.0,59.0,27.0);
                    }else{
                        if(vecidx>=90.0) return vec4(0.0,20.0,0.0,211.0);
                        if(vecidx>=89.0) return vec4(179.0,18.0,123.0,11.0);
                        if(vecidx>=88.0) return vec4(211.0,21.0,19.0,0.0);
                    }
                }else{
                    if(vecidx>=84.0){
                        if(vecidx>=87.0) return vec4(20.0,0.0,15.0,26.0);
                        if(vecidx>=86.0) return vec4(19.0,124.0,30.0,0.0);
                        if(vecidx>=85.0) return vec4(235.0,248.0,187.0,0.0);
                        if(vecidx>=84.0) return vec4(0.0,20.0,0.0,163.0);
                    }else{
                        if(vecidx>=83.0) return vec4(211.0,0.0,16.0,72.0);
                        if(vecidx>=82.0) return vec4(0.0,107.0,163.0,59.0);
                        if(vecidx>=81.0) return vec4(0.0,0.0,0.0,0.0);
                    }
                }
            }
        }else{
            if(vecidx>=67.0){
                if(vecidx>=74.0){
                    if(vecidx>=77.0){
                        if(vecidx>=80.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=79.0) return vec4(234.0,258.0,0.0,0.0);
                        if(vecidx>=78.0) return vec4(248.0,0.0,218.0,235.0);
                        if(vecidx>=77.0) return vec4(111.0,0.0,123.0,11.0);
                    }else{
                        if(vecidx>=76.0) return vec4(178.0,59.0,235.0,162.0);
                        if(vecidx>=75.0) return vec4(26.0,187.0,178.0,0.0);
                        if(vecidx>=74.0) return vec4(187.0,0.0,59.0,27.0);
                    }
                }else{
                    if(vecidx>=70.0){
                        if(vecidx>=73.0) return vec4(15.0,0.0,107.0,163.0);
                        if(vecidx>=72.0) return vec4(0.0,19.0,26.0,219.0);
                        if(vecidx>=71.0) return vec4(15.0,26.0,19.0,19.0);
                        if(vecidx>=70.0) return vec4(35.0,211.0,0.0,19.0);
                    }else{
                        if(vecidx>=69.0) return vec4(242.0,51.0,0.0,242.0);
                        if(vecidx>=68.0) return vec4(187.0,163.0,178.0,0.0);
                        if(vecidx>=67.0) return vec4(0.0,21.0,11.0,51.0);
                    }
                }
            }else{
                if(vecidx>=60.0){
                    if(vecidx>=63.0){
                        if(vecidx>=66.0) return vec4(219.0,0.0,178.0,235.0);
                        if(vecidx>=65.0) return vec4(35.0,218.0,21.0,107.0);
                        if(vecidx>=64.0) return vec4(0.0,0.0,0.0,178.0);
                        if(vecidx>=63.0) return vec4(211.0,0.0,0.0,0.0);
                    }else{
                        if(vecidx>=62.0) return vec4(211.0,178.0,187.0,3.0);
                        if(vecidx>=61.0) return vec4(3.0,27.0,59.0,27.0);
                        if(vecidx>=60.0) return vec4(0.0,107.0,187.0,19.0);
                    }
                }else{
                    if(vecidx>=57.0){
                        if(vecidx>=59.0) return vec4(0.0,178.0,114.0,187.0);
                        if(vecidx>=58.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=57.0) return vec4(11.0,0.0,0.0,0.0);
                    }else{
                        if(vecidx>=56.0) return vec4(0.0,200.0,27.0,21.0);
                        if(vecidx>=55.0) return vec4(0.0,218.0,27.0,26.0);
                        if(vecidx>=54.0) return vec4(15.0,21.0,178.0,32.0);
                    }
                }
            }
        }
    }else{
        if(vecidx>=27.0){
            if(vecidx>=40.0){
                if(vecidx>=47.0){
                    if(vecidx>=50.0){
                        if(vecidx>=53.0) return vec4(0.0,211.0,26.0,242.0);
                        if(vecidx>=52.0) return vec4(0.0,114.0,21.0,178.0);
                        if(vecidx>=51.0) return vec4(0.0,218.0,27.0,26.0);
                        if(vecidx>=50.0) return vec4(27.0,11.0,19.0,187.0);
                    }else{
                        if(vecidx>=49.0) return vec4(123.0,107.0,248.0,0.0);
                        if(vecidx>=48.0) return vec4(0.0,256.0,256.0,256.0);
                        if(vecidx>=47.0) return vec4(0.0,0.0,0.0,0.0);
                    }
                }else{
                    if(vecidx>=43.0){
                        if(vecidx>=46.0) return vec4(211.0,256.0,256.0,256.0);
                        if(vecidx>=45.0) return vec4(163.0,123.0,18.0,18.0);
                        if(vecidx>=44.0) return vec4(3.0,187.0,107.0,0.0);
                        if(vecidx>=43.0) return vec4(187.0,0.0,211.0,21.0);
                    }else{
                        if(vecidx>=42.0) return vec4(256.0,256.0,178.0,114.0);
                        if(vecidx>=41.0) return vec4(0.0,0.0,0.0,256.0);
                        if(vecidx>=40.0) return vec4(256.0,256.0,0.0,0.0);
                    }
                }
            }else{
                if(vecidx>=33.0){
                    if(vecidx>=36.0){
                        if(vecidx>=39.0) return vec4(248.0,27.0,15.0,256.0);
                        if(vecidx>=38.0) return vec4(0.0,30.0,21.0,211.0);
                        if(vecidx>=37.0) return vec4(218.0,235.0,234.0,35.0);
                        if(vecidx>=36.0) return vec4(187.0,248.0,211.0,0.0);
                    }else{
                        if(vecidx>=35.0) return vec4(178.0,0.0,107.0,187.0);
                        if(vecidx>=34.0) return vec4(26.0,178.0,0.0,21.0);
                        if(vecidx>=33.0) return vec4(0.0,0.0,0.0,242.0);
                    }
                }else{
                    if(vecidx>=30.0){
                        if(vecidx>=32.0) return vec4(90.0,256.0,0.0,0.0);
                        if(vecidx>=31.0) return vec4(0.0,72.0,91.0,73.0);
                        if(vecidx>=30.0) return vec4(248.0,0.0,21.0,11.0);
                    }else{
                        if(vecidx>=29.0) return vec4(248.0,123.0,178.0,187.0);
                        if(vecidx>=28.0) return vec4(178.0,0.0,234.0,59.0);
                        if(vecidx>=27.0) return vec4(0.0,162.0,123.0,211.0);
                    }
                }
            }
        }else{
            if(vecidx>=13.0){
                if(vecidx>=20.0){
                    if(vecidx>=23.0){
                        if(vecidx>=26.0) return vec4(35.0,187.0,123.0,248.0);
                        if(vecidx>=25.0) return vec4(15.0,0.0,178.0,114.0);
                        if(vecidx>=24.0) return vec4(51.0,235.0,35.0,26.0);
                        if(vecidx>=23.0) return vec4(0.0,0.0,123.0,0.0);
                    }else{
                        if(vecidx>=22.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=21.0) return vec4(256.0,0.0,0.0,0.0);
                        if(vecidx>=20.0) return vec4(123.0,3.0,19.0,114.0);
                    }
                }else{
                    if(vecidx>=16.0){
                        if(vecidx>=19.0) return vec4(187.0,0.0,211.0,187.0);
                        if(vecidx>=18.0) return vec4(27.0,27.0,219.0,18.0);
                        if(vecidx>=17.0) return vec4(11.0,178.0,0.0,219.0);
                        if(vecidx>=16.0) return vec4(11.0,27.0,19.0,187.0);
                    }else{
                        if(vecidx>=15.0) return vec4(11.0,0.0,21.0,11.0);
                        if(vecidx>=14.0) return vec4(178.0,114.0,0.0,123.0);
                        if(vecidx>=13.0) return vec4(211.0,0.0,30.0,21.0);
                    }
                }
            }else{
                if(vecidx>=6.0){
                    if(vecidx>=9.0){
                        if(vecidx>=12.0) return vec4(178.0,123.0,3.0,178.0);
                        if(vecidx>=11.0) return vec4(21.0,178.0,0.0,211.0);
                        if(vecidx>=10.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=9.0) return vec4(0.0,0.0,0.0,0.0);
                    }else{
                        if(vecidx>=8.0) return vec4(0.0,0.0,0.0,0.0);
                        if(vecidx>=7.0) return vec4(0.0,26.0,211.0,256.0);
                        if(vecidx>=6.0) return vec4(187.0,0.0,27.0,51.0);
                    }
                }else{
                    if(vecidx>=3.0){
                        if(vecidx>=5.0) return vec4(218.0,0.0,27.0,11.0);
                        if(vecidx>=4.0) return vec4(3.0,187.0,123.0,248.0);
                        if(vecidx>=3.0) return vec4(187.0,0.0,123.0,18.0);
                    }else{
                        if(vecidx>=2.0) return vec4(26.0,0.0,123.0,3.0);
                        if(vecidx>=1.0) return vec4(0.0,0.0,218.0,27.0);
                        if(vecidx>=0.0) return vec4(0.0,0.0,0.0,0.0);
                    }
                }
            }
        }
    }
    return vec4(0.0);
} //get_seg_vec4

#define NUM_CHARS_IN_MESSAGE (435.0)
float get_seg_mask(float charidx)
{
    if(charidx>=NUM_CHARS_IN_MESSAGE) return 0.0; //blank at the end
    float vecidx = charidx * 0.250000000;
    float subidx = mod(charidx, 4.0);
    vec4 v = get_seg_vec4(vecidx);
    float rv = v[0];
    rv = mix(rv, v[1], step(1.0, subidx));
    rv = mix(rv, v[2], step(2.0, subidx));
    rv = mix(rv, v[3], step(3.0, subidx));
    return rv;
} //get_seg_mask

// Camera and light prototypes

void do_cl_blank(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line1(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line2(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line3(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line4(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line5(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_line6(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_nameus(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_greet(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_credz(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_last1(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_ncps1(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_year1(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);
void do_cl_endpart(in float partnum, in float charidx_frac, out vec3 camera_pos, out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg, out vec3 light_pos);

void do_camera_light(in float partnum, in float charidx_frac,
                        out vec3 camera_pos,
                        out vec3 camera_look_at, out vec3 camera_up,
                        out float fovy_deg, out vec3 light_pos)
{   // Camera and light dispatcher
    if(partnum>=NAMEUS) {

        if(partnum==NAMEUS) {
            do_cl_nameus(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==GREET) {
            do_cl_greet(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==CREDZ) {
            do_cl_credz(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LAST1) {
            do_cl_last1(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==NCPS1) {
            do_cl_ncps1(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==YEAR1) {
            do_cl_year1(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==ENDPART) {
            do_cl_endpart(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    } else {

        if(partnum==BLANK) {
            do_cl_blank(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE1) {
            do_cl_line1(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE2) {
            do_cl_line2(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE3) {
            do_cl_line3(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE4) {
            do_cl_line4(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE5) {
            do_cl_line5(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        if(partnum==LINE6) {
            do_cl_line6(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else

        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    }
} //do_camera_light

#define NCPS1_REALSTART (420.00000000000000000000)
#define YEAR1_REALSTART (429.00000000000000000000)
#define YEAR1_CLIPCHARIDX (437.00000000000000000000)


// UTIL ///////////////////////////////////////////////
// {{{1
//mat4 my_transpose(in mat4 inMatrix)
//
//{
//    // Modified from
//    // http://stackoverflow.com/a/18038495/2877364 by
//    // http://stackoverflow.com/users/2507370/jeb
//    vec4 i0 = inMatrix[0];
//    vec4 i1 = inMatrix[1];
//    vec4 i2 = inMatrix[2];
//    vec4 i3 = inMatrix[3];
//
//    vec4 o0 = vec4(i0.x, i1.x, i2.x, i3.x);
//    vec4 o1 = vec4(i0.y, i1.y, i2.y, i3.y);
//    vec4 o2 = vec4(i0.z, i1.z, i2.z, i3.z);
//    vec4 o3 = vec4(i0.w, i1.w, i2.w, i3.w);
//
//    mat4 outMatrix = mat4(o0, o1, o2, o3);
//
//    return outMatrix;
//}

void lookat(in vec3 in_eye, in vec3 in_ctr, in vec3 in_up,
            out mat4 view, out mat4 view_inv)
{
    // From Mesa glu.  Thanks to
    // http://learnopengl.com/#!Getting-started/Camera
    // and https://www.opengl.org/wiki/GluLookAt_code

    vec3 forward, side, up;

    forward=normalize(in_ctr-in_eye);
    up = in_up;
    side = normalize(cross(forward,up));
    up = cross(side,forward);   // already normalized since both inputs are
        //now side, up, and forward are orthonormal

    mat4 orient, where;

    // Note: in Mesa gluLookAt, a C matrix is used, so the indices
    // have to be swapped compared to that code.
    vec4 x4, y4, z4, w4;
    x4 = vec4(side,0);
    y4 = vec4(up,0);
    z4 = vec4(-forward,0);
    w4 = vec4(0,0,0,1);
    orient = transpose(mat4(x4, y4, z4, w4));

    where = mat4(1.0); //identity (1.0 diagonal matrix)
    where[3] = vec4(-in_eye, 1);

    view = (orient * where);

    // Compute the inverse for later
    view_inv = mat4(x4, y4, z4, -where[3]);
    view_inv[3][3] = 1.0;   // since -where[3].w == -1, not what we want
        // Per https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations ,
        // M_{view->world}
} //lookat

void gluPerspective(in float fovy_deg, in float aspect,
                    in float near, in float far,
                    out mat4 proj, out mat4 proj_inv)
{   // from mesa glu-9.0.0/src/libutil/project.c.
    // Thanks to https://unspecified.wordpress.com/2012/06/21/calculating-the-gluperspective-matrix-and-other-opengl-matrix-maths/

    float fovy_rad = radians(fovy_deg);
    float dz = far-near;
    float sin_fovy = sin(fovy_rad);
    float cot_fovy = cos(fovy_rad) / sin_fovy;

    proj=mat4(0);
    //[col][row]
    proj[0][0] = cot_fovy / aspect;
    proj[1][1] = cot_fovy;

    proj[2][2] = -(far+near)/dz;
    proj[2][3] = -1.0;

    proj[3][2] = -2.0*near*far/dz;

    // Compute the inverse matrix.
    // http://bookofhook.com/mousepick.pdf
    float a = proj[0][0];
    float b = proj[1][1];
    float c = proj[2][2];
    float d = proj[3][2];
    float e = proj[2][3];

    proj_inv = mat4(0);
    proj_inv[0][0] = 1.0/a;
    proj_inv[1][1] = 1.0/b;
    proj_inv[3][2] = 1.0/e;
    proj_inv[2][3] = 1.0/d;
    proj_inv[3][3] = -c/(d*e);
} //gluPerspective

void compute_viewport(in float x, in float y, in float w, in float h,
                        out mat4 viewp, out mat4 viewp_inv)
{
    // See https://en.wikibooks.org/wiki/GLSL_Programming/Vertex_Transformations#Viewport_Transformation
    // Also mesa src/mesa/main/viewport.c:_mesa_get_viewport_xform()

    viewp = mat4(0);
    // Reminder: indexing is [col][row]
    viewp[0][0] = w/2.0;
    viewp[3][0] = x+w/2.0;

    viewp[1][1] = h/2.0;
    viewp[3][1] = y+h/2.0;

    // assumes n=0 and f=1,
    // which are the default for glDepthRange.
    viewp[2][2] = 0.5;  // actually 0.5 * (f-n);
    viewp[3][2] = 0.5;  // actually 0.5 * (n+f);

    viewp[3][3] = 1.0;

    //Invert.  Done by hand.
    viewp_inv = mat4(1.0);
    viewp_inv[0][0] = 2.0/w;    // x->x
    viewp_inv[3][0] = -1.0 - (2.0*x/w);

    viewp_inv[1][1] = 2.0/h;    // y->y
    viewp_inv[3][1] = -1.0 - (2.0*y/h);

    viewp_inv[2][2] = 2.0;      // z->z
    viewp_inv[3][2] = -1.0;

}  //compute_viewport

// https://www.opengl.org/wiki/Compute_eye_space_from_window_space

vec4 wts(in mat4 modelviewproj, in mat4 viewport,
                in vec3 pos)
{   // world to screen coordinates
    vec4 clipvertex = modelviewproj * vec4(pos,1.0);
    vec4 ndc = clipvertex/clipvertex.w;
    vec4 transformed = viewport * ndc;
    return transformed;
} //wts

// screen to world: http://bookofhook.com/mousepick.pdf
vec4 WorldRayFromScreenPoint(in vec2 scr_pt,
    in mat4 view_inv,
    in mat4 proj_inv,
    in mat4 viewp_inv)
{   // Returns world coords of a point on a ray passing through
    // the camera position and scr_pt.

    vec4 ndc = viewp_inv * vec4(scr_pt,0.0,1.0);
        // z=0.0 => it's a ray.  0 is an arbitrary choice in the
        // view volume.
        // w=1.0 => we don't need to undo the perspective divide.
        //      So clip coords == NDC

    vec4 view_coords = proj_inv * ndc;
        // At this point, z=0 will have become something in the
        // middle of the projection volume, somewhere between
        // near and far.
    view_coords = view_coords / view_coords.w;
        // Keepin' it real?  Not sure what happens if you skip this.
    //view_coords.w = 0.0;
        // Remove translation components.  Note that we
        // don't use this trick.
    vec4 world_ray_point = view_inv * view_coords;
        // Now scr_pt is on the ray through camera_pos and world_ray_point
    return world_ray_point;
} //WorldRayFromScreenPoint

vec3 hsv2rgb(vec3 c) {
    // by hughsk, from https://github.com/hughsk/glsl-hsv2rgb/blob/master/index.glsl .
    // All inputs range from 0 to 1.
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float scalesin(in float bot, in float top, in float x)
{   //rescale [-1,1] to [bot, top]
    return mix(bot, top, clamp((x+1.0)*0.5, 0.0, 1.0));
}



// }}}1

// VOXEL MARCHING
// {{{1

// GLSL implementation by cxw.
// Origial by http://gamedev.stackexchange.com/users/8806/maxim-kamalov , aka
// dogfuntom, https://gist.github.com/dogfuntom .
// See http://gamedev.stackexchange.com/questions/47362/cast-ray-to-select-block-in-voxel-game#comment188335_49423
// Modified from https://gist.github.com/cc881c8fc86ad43d55d8.git
// Heavily based on:
// http://gamedev.stackexchange.com/a/49423/8806

// Variables for voxel marching
struct VM2State {
    // Parameters
    vec3 origin;
    vec3 direction;
    vec3 world_min;
    vec3 world_max;

    // Internals
    vec3 curr;      //where we are now - was x, y, z vars
    vec3 stepdir;   //was step[XYZ]
    vec3 tMax;
    vec3 tDelta;
    float max_t;
}; //VM2State



/// Handle edge cases when initializing marching
float intbound(float s, float ds)
{
    // Some kind of edge case, see:
    // http://gamedev.stackexchange.com/questions/47362/cast-ray-to-select-block-in-voxel-game#comment160436_49423 :
        // "The edge case is where a coordinate of the ray origin is an
        //integer value, and the corresponding part of the ray direction is
        //negative. The initial tMax value for that axis should be zero, since
        //the origin is already at the bottom edge of its cell, but it is
        //instead 1/ds causing one of the other axes to be incremented instead.
        //The fix is to write intfloor to check if both ds is negative and s is
        //an integer value (mod returns 0), and return 0.0 in that case. –
        //codewarrior Dec 24 '14 at 12:00"

    // by http://gamedev.stackexchange.com/users/57468/codewarrior
    bool sIsInteger = (fract(s)==0.0);  //TODO check against epsilon?
    if (ds < 0.0 && sIsInteger)
        return 0.0;

    return (
        ( (ds > 0.0) ? (ceil(s) - s) : (s - floor(s)) )
            // I don't need intbound_ceil here since I make sure origin
            // always has a fractional part using hack_camera().
        /
        abs(ds)
    );
} //intbound

bool VM2_init(out VM2State state,
    in vec3 origin, in vec3 direction, in float max_dist,
    in vec3 world_min, in vec3 world_max)
{ //The initialization portion of VM2_raycast (q.v.).
  //Returns true if successful.

    if(length(direction)==0.0) {
        return false;   // *** EXIT POINT ***
    }

    state.origin = origin;
    state.direction = direction;
    state.world_min = world_min;     // TODO? make sure they are ints?
    state.world_max = world_max;

    state.curr = floor(origin);
    state.stepdir = sign(direction);

    state.tMax.x = intbound(origin.x, direction.x);
    state.tMax.y = intbound(origin.y, direction.y);
    state.tMax.z = intbound(origin.z, direction.z);

    state.tDelta.x = state.stepdir.x / direction.x;
    state.tDelta.y = state.stepdir.y / direction.y;
    state.tDelta.z = state.stepdir.z / direction.z;

    state.max_t = max_dist / length(direction);

    

    return true;
} //VM2_init

//DEBUG: these are floats.  For production, change them back to int.
#define VM2_HIT (1.0)
#define VM2_NOTYET (-1.0)
#define VM2_DONE (0.0)

vec4 VM2_step(inout VM2State state,
                out vec3 voxel, out vec3 hitpoint, out vec3 normal,
                out float hit_t)
{ //returns:
  // VM2_HIT    if we hit a voxel in the world;
  // VM2_NOTYET if we have not yet reached the world; or
  // VM2_DONE   if we have traced off the end of the world or have gone
  //            too far along the ray.
  // If VM2_HIT, voxel and normal are filled in:
  //    voxel       coordinates of the voxel we're in
  //    hitpoint    The actual point where the ray hit the voxel
  //    normal      normal of the voxel at hitpoint

    vec3 ret_normal;    //value to be returned

    // Go to the next voxel.
    //DEBUG: The *0.05's below are to rescale for visibility, and are debug.
    if (state.tMax.x < state.tMax.y) {
        if (state.tMax.x < state.tMax.z) {
            if (state.tMax.x > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.x += state.stepdir.x;    // Update which cube we are now in.
            hit_t = state.tMax.x;               // Record where we hit the cube
            state.tMax.x += state.tDelta.x;
                // Adjust state.tMax.x to the next X-oriented crossing
            ret_normal = vec3(-state.stepdir.x, 0.0, 0.0);
                // Record the normal vector of the voxel we just entered.
        } else {
            if (state.tMax.z > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.z += state.stepdir.z;
            hit_t = state.tMax.z;
            state.tMax.z += state.tDelta.z;
            ret_normal = vec3(0.0, 0.0, -state.stepdir.z);
        }
    } else {
        if (state.tMax.y < state.tMax.z) {
            if (state.tMax.y > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.y += state.stepdir.y;
            hit_t = state.tMax.y;
            state.tMax.y += state.tDelta.y;
            ret_normal = vec3(0.0, -state.stepdir.y, 0.0);
        } else {
            if (state.tMax.z > state.max_t) return vec4(state.tMax*0.05,VM2_DONE);
            state.curr.z += state.stepdir.z;
            hit_t = state.tMax.z;
            state.tMax.z += state.tDelta.z;
            ret_normal = vec3(0.0, 0.0, -state.stepdir.z);
        }
    } //end conditionals

    // Check if we're past the world
    if( (state.stepdir.x>0.0) ?
        (state.curr.x>=state.world_max.x) : (state.curr.x<state.world_min.x) )
        return vec4(1.0,0.0,0.0,VM2_DONE);
    if( (state.stepdir.y>0.0) ?
        (state.curr.y>=state.world_max.y) : (state.curr.y<state.world_min.y) )
        return vec4(0.0,1.0,0.0,VM2_DONE);
    if( (state.stepdir.z>0.0) ?
        (state.curr.z>=state.world_max.z) : (state.curr.z<state.world_min.z) )
        return vec4(0.0,0.0,1.0,VM2_DONE);

    // Check if we're not yet at the world.
    // TODO in VM2_init, fast-forward to the boundary of the world so that
    // this case never happens.
    if( (state.stepdir.x>0.0) ?
        (state.curr.x<state.world_min.x) : (state.curr.x>=state.world_max.x) )
        return vec4(0.5,0.0,0.0,VM2_NOTYET);
    if( (state.stepdir.y>0.0) ?
        (state.curr.y<state.world_min.y) : (state.curr.y>=state.world_max.y) )
        return vec4(0.0,0.5,0.0,VM2_NOTYET);
    if( (state.stepdir.z>0.0) ?
        (state.curr.z<state.world_min.z) : (state.curr.z>=state.world_max.z) )
        return vec4(0.0,0.0,0.5,VM2_NOTYET);

    // If we made it here, we are in a voxel cell.
    voxel = state.curr;
    hitpoint = state.origin + hit_t*state.direction;
    normal = ret_normal;
    return vec4(voxel,VM2_HIT);     //voxel is debug
} //VM2_step

// }}}1

// GEOMETRY HIT-TESTING ///////////////////////////////
// {{{1



/// Faster routine for the special case of the main text
vec3 HitZZero(vec3 camera_pos, vec3 rayend)
{   // Find where the ray meets the z=0 plane.  The ray is
    // camera_pos + t*(rayend - camera_pos) per Hook.
    float hit_t = -camera_pos.z / (rayend.z - camera_pos.z);
    return (camera_pos + hit_t * (rayend-camera_pos));
} //HitZZero

/// HitZZero(), but using a direction vector.
void HitZZeroDir(in vec3 camera_pos, in vec3 ray_dir,
                out vec3 hit_pos, out float hit_t)
{   // Find where the ray meets the z=0 plane.  The ray is
    // camera_pos + t*(rayend - camera_pos) per Hook.
    hit_t = -camera_pos.z / ray_dir.z;
    hit_pos = (camera_pos + hit_t * ray_dir);
} //HitZZeroDir

/// HitZZeroDir(), but for z!=0
void HitZDir(in vec3 camera_pos, in vec3 ray_dir, in float z,
                out vec3 hit_pos, out float hit_t)
{   // Find where the ray meets the z=0 plane.  The ray is
    // camera_pos + t*(rayend - camera_pos) per Hook.
    hit_t = (z-camera_pos.z) / ray_dir.z;
    hit_pos = (camera_pos + hit_t * ray_dir);
} //HitZDir

// --- IsPointInRectXY ---
// All polys will be quads in the X-Y plane, Z=0.
// All quad edges are parallel to the X or Y axis.
// These quads are encoded in a vec4: (.x,.y) is the LL corner and
// (.z,.w) is the UR corner (coords (x,y)).  The UR corner is not
// inclued in the poly.

bool IsPointInRectXY(in vec4 poly_coords, in vec2 world_xy_of_point)
{
    // return true if world_xy_of_point is within the poly defined by
    // poly_coords in the Z=0 plane.
    // I.e., xy >= poly_coords.xy, and xy < poly_coords.zw.
    // I can test in 2D rather than 3D because all the geometry
    // has z=0 and all the quads are planar.

    float x_test, y_test;
    x_test = step(poly_coords.x, world_xy_of_point.x) *
            (1.0 - step(poly_coords.z, world_xy_of_point.x));
        // step() is 1.0 if world.x >= poly_coords.x
        // 1-step() is 1.0 if world.x < poly_coords.z
    y_test = step(poly_coords.y, world_xy_of_point.y) *
            (1.0 - step(poly_coords.w, world_xy_of_point.y));

    return ( (x_test>=0.9) && (y_test >= 0.9) );
        // Not ==1.0 because these are floats!

} //IsPointInRectXY



// }}}1

// TEXT RENDERING /////////////////////////////////////
// {{{1

// Text-rendering internal parameters
#define LETTER_EPSILON (0.001)
    // small enough for our purposes.
#define SIDE_LETTERS (4)
    // How many letters to render on each side of the current one.
    // Set to fill the screen at the desired aspect ratio and orientation.



bool is_in_basic_message(in vec2 in_pt,
    in float first_charidx, in float clip_charidx,
    in vec2 text_origin)
{   // returns true iff world_xy_of_point is the message for this part,
    // which begins with first_charidx at text_origin,
    // upright in the z=0 plane.

    vec2 pt = in_pt - text_origin;     // adjust for where the text is

    float nchars = (clip_charidx-first_charidx);
        //not ()+1 because clip_charidx is one past the last char to show.
    if( (pt.x<0.0) || (pt.x>=nchars*GRID_PITCH) ) {
        return false;   //outside - can't hit
    }

    if( (pt.y<0.0) || (pt.y>GRID_CHARHT*GRID_VPITCH) ) {
        return false;   //ditto
    }

    // Which letter are we in?  There can be only one.
    float ltridx = floor(pt.x/GRID_PITCH);
    float ofs = ltridx * GRID_PITCH;
    vec2 shifted_pt = pt - vec2(ofs,0.0);   // with respect to the letter
    float mask = get_seg_mask(first_charidx + ltridx);

    // Early exit on spaces
    if(mask <= LETTER_EPSILON) {
        return false;
    }

    // check each segment in turn
    for(int seg_idx=0; seg_idx<NSEGS; ++seg_idx) {
        if(mod(mask, 2.0)>LETTER_EPSILON) {
            // Where is this segment of this letter?
            vec4 theshape = SEG_SHAPES[seg_idx];

            // Check if we are in the segment
            if(IsPointInRectXY(theshape, shifted_pt)) {
                return true;    // as soon as we're in a segment,
            }                   // we don't need to check any others

        } //endif this segment is in mask

        mask = floor(mask * 0.5);
            //move to next bit and drop fractional part

        // Early exit when you run out of segments
        if(mask<=LETTER_EPSILON) {
            return false;       // no more chances
        }
    } //foreach segment

    return false;
} //is_in_basic_message

// }}}1

// VOXEL LOGO /////////////////////////////////////////
// {{{1

// Voxel-Logo parameters.  Note: Voxels are every 1 unit at present.
#define VLOGO_HEIGHT (8.0)
#define VLOGO_Y_MAX (VLOGO_HEIGHT - 1.0)
#define VLOGO_WIDTH (7.0)
#define VLOGO_X_MAX (VLOGO_WIDTH - 1.0)
#define VLOGO_THICKNESS (1.0)
    // along the X axis

/// Determine whether the ray from #ray_origin in #ray_direction hits the logo.
/// The logo is in the X = #logo_origin.x plane, beginning at #logo_origin.yz.
///
/// @return {bvec2} (did_hit, was_error)
/// did_hit is true on hit.  If true, _voxel_ holds the grid coordinates of
/// the voxel that was hit, _hitpoint_ holds the actual point hit, and
/// _normal_ is the normal of the voxel face that was hit.
/// was_error is true on error.
///
bvec2 voxel_is_in_logo(
    in vec3 ray_origin, in vec3 ray_direction,
    in vec3 logo_origin,
    out vec3 voxel, out vec3 hitpoint, out vec3 normal,
    out float hit_t)
{

    //Retval storage, so we don't trash the out parameters if there's no hit.
    vec3 ret_voxel, ret_hitpoint, ret_normal;
    float ret_hit_t;

    // TODO RESUME HERE double the size of the logo so it fills the screen
    // better during the intropart

    // Setup voxel marching
    bool ok;
    VM2State state;
    ok = VM2_init(state, ray_origin, ray_direction, MAX_DIST,
        //world_min
        logo_origin,
        //world_max - last+1
        logo_origin + vec3(VLOGO_THICKNESS, VLOGO_HEIGHT, VLOGO_WIDTH)
    );

    if(!ok) return bvec2(false, true);
        // *** EXIT POINT *** can't init => can't hit

    // Run the marching loop.  At each voxel, check the mask for only
    // the letter that voxel might be in.
    for(int step_idx=0; step_idx<MAX_VOXEL_STEPS; ++step_idx) {
        vec4 hit = VM2_step(state, ret_voxel, ret_hitpoint, ret_normal,
                                    ret_hit_t);

        if(hit.w == VM2_DONE) return bvec2(false, false); // *** EXIT POINT ***

        if(hit.w == VM2_NOTYET) continue;     // to the next voxel step

        // If we got here, we are in a voxel that is in the world.
        bool did_hit = false;

        // Don't test X, because the "are we in the world" test covers that.
        // We just need to test Z across the width and Y across the height.
        // Also, shift from voxel coordinates into logo-relative coords.
        float x = ret_voxel.z - logo_origin.z;
        float y = ret_voxel.y - logo_origin.y;

        // Sanity check.  Do we need this?
        if(x < 0.0 || y<0.0 || x>VLOGO_X_MAX || y>VLOGO_Y_MAX) continue;

        /// Distance from the closest edges in X and Y
        float xe = min(x, abs(VLOGO_X_MAX - x));
        float ye = min(y, abs(VLOGO_Y_MAX - y));

        // TODO check here to see if voxel (x,y) is in the logo

        if(did_hit) {
           // A hit!  A very palpable hit.
           voxel = ret_voxel;
           hitpoint = ret_hitpoint;
           normal = ret_normal;
           hit_t = ret_hit_t;
           return bvec2(true, false);   // did hit; no error
        }

    } //for each voxel step

    return bvec2(false, false);     // didn't hit, but no error.
} //voxel_is_in_logo





// }}}1

// BUMP MAPPING ///////////////////////////////////////
// {{{1

vec3 bump_normal(in sampler2D unit_wave, in vec2 tex_coord)
{
// https://stackoverflow.com/a/5284527/2877364 by
// https://stackoverflow.com/users/607131/kvark
    //float center = texture(iChannel0, hitlocn.xy * TEX_SCALE).ggg;
    const vec2 size = vec2(2.0,0.0);
    const ivec3 off = ivec3(-1,0,1);

    vec4 wave = texture(unit_wave, tex_coord);
    float s11 = wave.x;
    float s01 = textureOffset(unit_wave, tex_coord, off.xy).x;
    float s21 = textureOffset(unit_wave, tex_coord, off.zy).x;
    float s10 = textureOffset(unit_wave, tex_coord, off.yx).x;
    float s12 = textureOffset(unit_wave, tex_coord, off.yz).x;
    vec3 va = normalize(vec3(size.xy,s21-s01));
    vec3 vb = normalize(vec3(size.yx,s12-s10));
    //vec4 bump = vec4( cross(va,vb), s11 );
    //The result is a bump vector: xyz=normal, a=height
    return cross(va,vb);
} //bump_normal

// }}}1

// CAMERA AND LIGHT ///////////////////////////////////
// {{{1

// --- Helpers ---

#define GAMMA (2.2)
#define ONE_OVER_GAMMA (0.45454545454545454545454545454545)

vec3 phong_color(
    in vec3 pixel_pos, in vec3 normal, in vec3 camera_pos,      // Scene
    in vec3 light_pos, in vec3 ambient_matl,                    // Lights
    in vec3 diffuse_matl, in vec3 specular_matl,                // Lights
    in float shininess)                                         // Material
{   // Compute pixel color using Blinn-Phong shading with a white light.
    // Modified from
    // https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model
    // Normal must be normalized on input.  All inputs are world coords.
    // Set shininess <=0 to turn off specular highlights.
    // Objects are one-sided.

    vec3 light_dir = normalize(light_pos - pixel_pos);
    vec3 eye_dir = normalize(camera_pos - pixel_pos);

    if(dot(light_dir, eye_dir) < 0.0) {
        return ambient_matl;       // Camera behind the object
    }

    float lambertian = max(0.0, dot(light_dir, normal));        // Diffuse

    float specular = 0.0;
    if((lambertian > 0.0) && (shininess > 0.0)) {               // Specular
        vec3 reflectDir = reflect(-light_dir, normal);
        float specAngle = max(dot(reflectDir, eye_dir), 0.0);
        specular = pow(specAngle, shininess);
    }
    lambertian = pow(lambertian, ONE_OVER_GAMMA);
    specular = pow(specular, ONE_OVER_GAMMA);

    vec3 retval = ambient_matl + lambertian*diffuse_matl +
        specular*specular_matl;

    return clamp(retval, 0.0, 1.0);     // no out-of-range values, please!

} //phong_color

highp vec3 pos_clelies(in float the_time, in float radius)
{   //Clelies curve
    //thanks to http://wiki.roblox.com/index.php?title=Parametric_equations
    vec3 pos; float m = 0.8;
    highp float smt = sin(m*the_time);
    pos.x = radius * smt*cos(the_time);
    pos.y = radius * smt*sin(the_time);
    pos.z = radius * cos(m*the_time);
    return pos;
} //camerapos

/// Get the position of moving text in the main parts.
void get_text_origin(
    //in float partnum,
    in float charidx_frac, out vec3 text_origin
    //out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
    //out vec3 light_pos
    )
{
    text_origin = vec3(-charidx_frac*GRID_PITCH-5.0, 0.0, 0.0);
        // Origin moves to the left, so the view pans to the right.
}

// --- Per-part routines referenced above ---

void do_cl_nop(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(0.0,0.0,10.0);    //default
    camera_look_at = vec3(0.0);
    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
} //do_cl_nop

void do_cl_main(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    // Old lines - for moving camera
    //camera_pos = vec3(charidx_frac*GRID_PITCH-5.0, GRID_CHARHT*0.5, 10.0);
    //camera_look_at = vec3(camera_pos.x+3.0, GRID_CHARHT*0.5,0);

    // New lines - for use with moving text
    camera_pos = vec3(-5.0, GRID_CHARHT*0.5, 10.0);
    camera_look_at = vec3(-5.0+3.0, GRID_CHARHT*0.5,0);

    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 45.0;
    light_pos = camera_pos;
    light_pos.y += 4.0 * sin(charidx_frac);
} //do_cl_main

void do_cl_intro(in float pn, in float cf, out vec3 camera_pos, out vec3 cla, out vec3 cu, out float fovy_deg, out vec3 light_pos) {
    do_cl_main(pn, cf, camera_pos, cla, cu, fovy_deg, light_pos);
    fovy_deg = 39.0;    // a bit tighter for the intro, so we can see
                        // the logo better.
    light_pos = camera_pos; // TODO improve this later.
} //do_cl_intro




void do_cl_blank(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_line1(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_line2(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_line3(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_line4(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_line5(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_line6(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_nameus(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_greet(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }


void do_cl_credz(in float partnum, in float charidx_frac, out vec3 camera_pos,
     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
     out vec3 light_pos)
{
    camera_pos = vec3(0.0, GRID_CHARHT*0.5, 10.0);
    camera_look_at = vec3(0.0-2.0, GRID_CHARHT*0.5,0);

    camera_up = vec3(0.0, 1.0, 0.0);
    fovy_deg = 50.0;
    light_pos = camera_pos;
    light_pos.y += 4.0 * sin(charidx_frac);
} //do_cl_main

void do_cl_last1(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_ncps1(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_year1(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }

void do_cl_endpart(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_main(pn, cf, cp, cla, cu, fd, lp); }


//void do_cl_xport(in float partnum, in float charidx_frac, out vec3 camera_pos,
//     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
//     out vec3 light_pos)
//{ //static camera
//    camera_pos = vec3(/*charidx_frac*/
//        floor(XPORT_NCHARS/2.0)*GRID_PITCH+GRID_PITCH*0.3, GRID_CHARHT*0.5 + 0.5, 10.0);
//    camera_look_at = vec3(camera_pos.x, GRID_CHARHT*0.5,0);
//    camera_up = vec3(0.0, 1.0, 0.0);
//    fovy_deg = 68.0;
//    light_pos = camera_pos;
//} //do_cl_xport
//
//void do_cl_line2(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_s_plain(pn, cf, cp, cla, cu, fd, lp); }
//
//void do_cl_line3(in float partnum, in float charidx_frac, out vec3 camera_pos,
//     out vec3 camera_look_at, out vec3 camera_up, out float fovy_deg,
//     out vec3 light_pos)
//{ //For the voxel part.
//    camera_pos = vec3(charidx_frac*VGRID_PITCH-5.0, VGRID_CHARHT*0.5+0.5, 20.0);
//    camera_look_at = vec3(camera_pos.x-0.5, VGRID_CHARHT*0.5,0.0);
//
//    camera_up = vec3(0.0, 1.0, 0.0);
//    fovy_deg = 45.0;
//    light_pos = vec3(
//        camera_pos.x + VGRID_PITCH*sin(TWO_PI*0.125*charidx_frac),
//        camera_pos.y+6.0,
//        camera_pos.z-2.0
//    );
//} //do_cl_line3
//
//void do_cl_howto(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_line3(pn, cf, cp, cla, cu, fd, lp); }
//
//void do_cl_endpart(in float pn, in float cf, out vec3 cp, out vec3 cla, out vec3 cu, out float fd, out vec3 lp) { do_cl_nop(pn, cf, cp, cla, cu, fd, lp); }



/// Make sure the camera is never at an integer coordinate.
/// TODO someday --- fix the voxel marcher ;)
vec3 hack_camera(vec3 camera_pos)
{
    return vec3(    fract(camera_pos.x) >= SMALLEST_CAM_FRACTION ?
        camera_pos.x :
        (
            (abs(camera_pos.x) - fract(camera_pos.x) +
                    SMALLEST_CAM_FRACTION)
            *
            sign(camera_pos.x)
        )
,     fract(camera_pos.y) >= SMALLEST_CAM_FRACTION ?
        camera_pos.y :
        (
            (abs(camera_pos.y) - fract(camera_pos.y) +
                    SMALLEST_CAM_FRACTION)
            *
            sign(camera_pos.y)
        )
,     fract(camera_pos.z) >= SMALLEST_CAM_FRACTION ?
        camera_pos.z :
        (
            (abs(camera_pos.z) - fract(camera_pos.z) +
                    SMALLEST_CAM_FRACTION)
            *
            sign(camera_pos.z)
        )
);
}


// }}}1

// ARTISTRY ///////////////////////////////////////////
// {{{1

void do_hey(in float the_time, in vec2 charpos, in float clip_charidx,
            out vec3 diffuse_matl)
{
    vec2 pos;
    diffuse_matl = vec3(0.0);
    vec2 text_origin = vec2(GRID_PITCH * (HEY_X_CELLS - 4.0 - 1.0), 0.0);
        // Multiply by GRID_PITCH ecause text_origin is in logical units,
        // not grid cells.
        // We have four characters, so leave four spaces, plus one at the
        // right for a margin.

    // Flicker minimum - starts at 1 (no flicker); gradually drops to 0.2.
    float flicker_depth =
            1.0 - 0.8 * smoothstep(YEAR1_START, YEAR1_START+5.0, the_time);

    float global_fadeout =
            smoothstep(NCPS1_START, NCPS1_START+2.0, the_time) *
            (1.0 - smoothstep(ENDPART_START - 3.0, ENDPART_START, the_time));

    //NCPS
    pos = charpos - vec2(0.0, 1.0*GRID_VPITCH); // move up one line
    if(is_in_basic_message(pos, NCPS1_REALSTART, clip_charidx, text_origin)) {
        diffuse_matl = vec3(1.0,0.27058823529411762943,0.0);
            // orangered from
            // https://www.rapidtables.com/web/color/orange-color.html

        // Fuzz the logo randomly for a flickery effect
        if(rand(the_time * 0.01) > 0.7) {
            diffuse_matl *= clamp(rand(the_time*0.2), flicker_depth, 1.0);
        }
        diffuse_matl *= global_fadeout;
        return;
    }

    // 2018
    pos = charpos; // - vec2(0.0, 0.0*GRID_VPITCH);
    if( is_in_basic_message(pos, YEAR1_REALSTART, clip_charidx, text_origin)) {
        diffuse_matl = vec3(0.54117647058823525885,0.02745098039215686236,0.02745098039215686236);
            // blood red from http://www.colourlovers.com/color/8A0707/Blood_red

        // mul by PI just to shift it.  Multiplicative rather than additive
        // because if it's additive, whatever happens to NCPS happens to
        // 2018 shortly after that.
        if(rand(the_time * 0.01 * PI) > 0.7) {
            diffuse_matl *= clamp(rand(the_time*0.2 * PI), flicker_depth, 1.0);
        }
        diffuse_matl *= global_fadeout;
        return;
    }
} //do_hey

//void color_xport(in vec2 wpt, in vec2 uvpt, in float dt,
//                out vec3 ambient_matl, out vec3 diffuse_matl,
//                out float shininess)
//{ // World point, Fragment point ([0,1] range), time within the effect.
//  // Fades in over time=[0, XPORT_FADEIN_DURATION].
//
//    float ramp = smoothstep(0.0, XPORT_FADEIN_DURATION, dt);
//    ambient_matl = vec3(0.0);   //for now
//
//    // Shininess
//    float s_dt = scalesin(-PI_OVER_2, PI_OVER_2, dt/XPORT_FADEIN_DURATION);
//        // s_dt goes from 0 to pi/2
//    float s_top = mix(XP_SHINE_HIGH, XP_SHINE_AIM, dt/XPORT_FADEIN_DURATION);
//    float s_bot = mix(XP_SHINE_LOW, XP_SHINE_AIM, dt/XPORT_FADEIN_DURATION);
//    float shine = sin(TWO_PI*XP_SHINE_HZ*dt);
//    shininess = scalesin(s_bot, s_top, shine); // [XP_SHINE_LOW, X~_HIGH]
//
//    // Overall gain
//    float g_top = clamp(ramp*1.5, 0.0, 1.0);
//    float g_bot = ramp*ramp;    // <=ramp, so <= g_top
//    float gain = sin(TWO_PI*XP_GAIN_HZ*dt);
//    gain = scalesin(g_bot, g_top, gain);    // [0,1]
//
//    // Localized gain - horizontal
//    float s;
//    < ? for my $idx (0..$#hs) {
//        my $lr = $hs[$idx];?>
//        s = sin(TWO_PI*XP_H< ?=$idx?>_PER_U * uvpt.x +
//            TWO_PI*XP_H< ?=$idx?>_PHASE +
//            TWO_PI*XP_H< ?=$idx?>_PHASE_PER_SEC * dt);
//        s = mix(s, 1.0, ramp);     // gain effects are 1.0 at the end
//        gain *= scalesin(0.0, 1.0, s);
//    < ? } ?>
//
//    diffuse_matl = gain * vec3(0.2, 0.2, 1.0);
//} //color_xport

bool do_voxel_logo(
    in vec3 camera_pos, in vec3 raydir_norm, in vec3 logo_origin,
    // Outputs - only updated if it hits.
    out vec3 wc_pixel, out vec3 wc_normal,
    out vec3 ambient_matl, out vec3 diffuse_matl,
    out float shininess,
    out float voxel_hit_t)
{
    bool did_hit = false;
    vec3 voxel, hitpoint, normal;
    float hit_t;
    bvec2 hit_status = voxel_is_in_logo(
        camera_pos, raydir_norm, logo_origin,
        // Outputs
        voxel, hitpoint, normal, hit_t
    );

    bool did_hit_voxel = hit_status.x;
    //bool was_error = hit_status.y;

    //if(was_error) {
    //    diffuse_matl = vec3(1.0, 0.0, 0.0);
    //} else
    if(did_hit_voxel) {
        did_hit = true;
        wc_pixel = hitpoint;
        wc_normal = normal;
        voxel_hit_t = hit_t;
        ambient_matl = vec3(0.1, 0.1, 0.1);
        diffuse_matl = vec3(0.2, 0.3, 0.8);

        shininess = 25.0;
    }

    return did_hit;

} // do_voxel_logo

void renderItAll( in vec4 story, in float the_time, in vec2 uv,
                    out vec4 fragColor, in vec2 fragCoord )
{
    init_charset();

    // --- Story ---
    float partnum=story[0], charidx_frac=story[1];
    float first_charidx=story[2], clip_charidx=story[3];

    // --- Camera and light ---
    vec3 camera_pos, camera_look_at, camera_up, light_pos;
    float fovy_deg;

    do_camera_light(partnum, charidx_frac,
        camera_pos, camera_look_at, camera_up, fovy_deg, light_pos);
    camera_pos = hack_camera(camera_pos);

    // Camera processing
    mat4 view, view_inv;

    //camera_look_at.y -= 5.0;    //DEBUG
    lookat(camera_pos, camera_look_at, camera_up,
            view, view_inv);

    mat4 proj, proj_inv;
    //fovy_deg = 25.0;    //DEBUG
    gluPerspective(fovy_deg, iResolution.x/iResolution.y, 1.0, 10.0,
                    proj, proj_inv);

    mat4 viewport, viewport_inv;
    compute_viewport(0.0, 0.0, iResolution.x, iResolution.y,
                        viewport, viewport_inv);

    // --- Geometry ---

    vec3 rayend = WorldRayFromScreenPoint(fragCoord,
                                    view_inv, proj_inv, viewport_inv).xyz;
    vec3 raydir_norm = normalize(rayend - camera_pos);

    // Each part determines world coords of the hit, normal at the
    // hit point, and base color of the geometry.

    vec3 wc_pixel;  // world coords of this pixel
    vec3 wc_normal; // ditto for the normal
    vec3 ambient_matl = vec3(0.1);
    vec3 diffuse_matl = vec3(0.0);
        // material - light is always white.  Alpha is always 1.
    float shininess = 4.0;  //Phong shininess
    bool did_hit = false;   //if not did_hit, just use _diffuse_.

    if ((partnum == BLANK) || (partnum == ENDPART) ) {    // black screens
        diffuse_matl = vec3(0.0);
    } else

    if( (partnum == NCPS1) || (partnum == YEAR1) ) {  // Static text
        // Straight 2d
        // Grid for this part is 9 chars across and 3 high,
        // offset by half a character vertically.
        vec2 charpos = uv * vec2(HEY_X_CELLS,HEY_Y_CELLS);
            //now charpos is 0..8 horz and 0..3 vert
        charpos.y -= HEY_Y_VOFS;    // now -0.5..2.5 vert are on screen
        charpos *= vec2(GRID_PITCH, GRID_VPITCH);
            // Now in coordinates of the segments

        do_hey( the_time, charpos,
                YEAR1_CLIPCHARIDX,  //clip_charidx,
                diffuse_matl);
            //did_hit stays false so we just use diffuse_matl.

    } else

    { // scrollers
        bool did_hit_message=false, did_hit_voxel=false;

        // Message outputs
        float message_hit_t;
        vec3 message_hit_locn;

        // Voxel outputs
        float voxel_hit_t;
        vec3 vwc_pixel, vwc_normal, vambient_matl, vdiffuse_matl;
        float vshininess;

        // Do the message

        vec3 text_origin;
        get_text_origin(charidx_frac, text_origin);
            // The letters move, not the camera

        HitZDir(camera_pos, raydir_norm, MESSAGE_Z,
            message_hit_locn, message_hit_t);
            // message_hit_locn is where it hits z=MESSAGE_Z,
            // where the letters are.  For now, ignore text_origin.z.

        // Swirly thing in the back
        if( (partnum != LINE1) && (partnum != NAMEUS) ) {
            // Map the Cartesian hit location to a polar hit location
            vec2 polar;
            message_hit_locn.y -= 12.0; //move the shape up on to the screen
            float theta = atan(message_hit_locn.y, message_hit_locn.x);
                // theta is on [-pi, pi]
            float r = length(message_hit_locn.xy - ARC_CENTER);
            polar.y = r - 5.0; //ARC_DEAD_RADIUS;   //DEBUG
            polar.x = theta + PI * 20.0;   //DEBUG test

            // Check the hit in the polar system
            did_hit_message = is_in_basic_message(polar, first_charidx,
                                clip_charidx, text_origin.xy*vec2(0.15,1.0));
                                    // slow down the spinning     ^^^^
        }

        // Now check the actual text, in screen space
        did_hit_voxel = is_in_basic_message(
            vec2(uv.x, uv.y)*vec2(25.0, 14.0)+vec2(0.0, -2.0), first_charidx,
                            clip_charidx, text_origin.xy);

        if(did_hit_voxel) {
            did_hit_message = false;        // Real message wins

            vwc_pixel = vec3(uv, 0.0);
            vwc_normal = vec3(0.0,0.0,1.0); //out of the screen
            vshininess = 25.0;
            vambient_matl = vec3(0.0);
            if(partnum == LINE1) {
                vdiffuse_matl = vec3(1.0,0.0,0.0);
            } else if(partnum == NAMEUS) {
                vdiffuse_matl = vec3(0.37254901960784314596, 0.49803921568627451677, 0.08235294117647058709);
            } else {
                vdiffuse_matl = vec3(0.1, 0.2, 0.8);
            }
        }

//        // Painter's algorithm (sort of)
//        if(did_hit_message && did_hit_voxel) {
//            if(message_hit_t < voxel_hit_t) {
//                did_hit_voxel = false;
//            } else {
//                did_hit_message = false;
//            }
//        }

        // Now at most one of message or voxel was hit.

        if(did_hit_message) {
            did_hit = true;
            wc_pixel = message_hit_locn;
            wc_normal = vec3(0.0,0.0,-1.0 + 2.0*step(0.0, camera_pos.z));
                // normal Z is -1 if camera_pos.z<0.0, and +1 otherwise.
                // This benefits TWOSIDED.

            // Perturb the normal by bump-mapping
            wc_normal = normalize(
                wc_normal + bump_normal(iChannel0,
                                            message_hit_locn.xy * TEX_SCALE)
            );
            ambient_matl = vec3(0.2, 0.2, 0.1);
            diffuse_matl = vec3(0.6,0.6,0.3);
            shininess = 50.0;

        } else if(did_hit_voxel) {
            did_hit = true;
            wc_pixel = vwc_pixel;
            wc_normal = vwc_normal;
            ambient_matl = vambient_matl;
            diffuse_matl = vdiffuse_matl;
            shininess = vshininess;
        }  // else diffuse is the default (0,0,0).

    }

    // --- Lighting ---
    // Phong shading based on the Geometry section's output values

    if(did_hit) {               // a hit
        vec3 rgb = phong_color(
            wc_pixel, wc_normal, camera_pos, light_pos,
            ambient_matl, diffuse_matl, vec3(1.0), shininess);

        fragColor = vec4(rgb, 1.0);
    } else {                    // no hit - just use diffuse_matl
        fragColor = vec4(diffuse_matl, 1.0);
    }
} //renderItAll

// }}}1

// MAIN ///////////////////////////////////////////////
// {{{1

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Init
    float the_time = iTime + START_OFS_SEC; //0.5*mod(iGlobalTime, 16.0)+S_PLAIN_START;
    //the_time += (NCPS1_START - 2.0); //DEBUG

    // Basic things we need
    vec2 uv = fragCoord.xy/iResolution.xy;
    vec4 story = get_story(the_time);
    float partnum = story[0];




    // BufA: render all
    renderItAll(story, the_time, uv, fragColor, fragCoord);


} //mainImage

// }}}1

// vi: set ts=4 sts=4 sw=4 et ai foldmethod=marker foldenable foldlevel=0: //
