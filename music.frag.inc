//music.frag.inc: Music parameters and other common foo ##########
<?# Don't forget to update gen-gfx.py when you change this! ?>
<? my $bpm = 270.0; my $startofs_beats = 0; #19; ?>

<?#----------------------------------------------?>

<?  use Math::Trig; use POSIX; ?>

<?  # Computed values
    my $bps = $bpm/60.0; my $spb = 60.0/$bpm;
    sub pf { return sprintf("%.20f",shift); }   # convenience - Print Float
    sub rgb_from_hex {
        my $hex = shift =~ s/^#//r;
        my @rgb = map $_ / 255, unpack 'C*', pack 'H*', $hex;
            # https://stackoverflow.com/a/17603124/2877364 by
            # https://stackoverflow.com/users/589924/ikegami
        return sprintf("vec3(%.20f, %.20f, %.20f)", $rgb[0], $rgb[1], $rgb[2]);
    } #rgb_from_hex
?>

#define BPM (<?=pf($bpm)?>)
    // beats per minute
#define BPS (<?= pf($bps) ?>)
    // beats per sec = BPM/60 (110 bpm)
#define SPB (<?= pf($spb) ?>)
    // sec per beat, precomputed
#define BPP (2048.0)
    // beats per pattern - make it longer than the song
    // if you don't want repeats

// DEBUG: where in the demo you want to start.
#define START_OFS_SEC <?= pf($startofs_beats * $spb) ?>

// Utils //////////////////////////////////////////////////////////////////
float rand(float n){    // http://shadertoy.wikia.com/wiki/Noise
    // added abs - make sure it's 0..1
    return fract(abs(cos(n*89.42))*343.42);
}
//################################################################

<?# vi: set ts=4 sts=4 sw=4 et ai foldmethod=marker foldenable foldlevel=0 ft=glsl330: ?>
