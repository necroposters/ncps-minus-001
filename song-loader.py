# song-loader.py

import sys
from textwrap import indent, dedent
from util import *

#import csv
#
#tempo =
#
#with open('song2.csv', newline='') as f:
#    reader = csv.reader(f)
#    for row in reader:
#        #print(row)
#        sTrack = row[0].strip()
#        sTime = row[1].strip()
#        sType = row[2].strip()
#        sChannel = row[3].strip()
#        sNote = row[4].strip()
#        sVolume = row[5].strip()
#
#        if not sTrack.isnumeric(): continue     #skip header row
#        track = int(sTrack)
#        if  (   (track==2 or track==3 or track==4) and
#                (sType=='Note_on_c' or sType=='Note_off_c')
#            ):
#            #if track not in tracks: tracks[track]=[]
#            tracks[track].append({'time':int(sTime), 'on':(sType=='Note_on_c'),
#                'channel':int(sChannel), 'note':int(sNote),
#                'volume':int(sVolume)})
#        #endif it's an event we want
#
#print(tracks)

import sys
sys.path.insert(0,'/home/ChrisW/src/mido/build/lib')
    # https://stackoverflow.com/a/12476364/2877364 by
    # https://stackoverflow.com/users/923794/cfi
import mido

mid = mido.MidiFile('song2.mid', type=1)
print('//Length %f; ticks_per_beat %f'%(mid.length, mid.ticks_per_beat))
tempo_bpm = 270 # from midi - OpenMPT says 135
ppb=480.0 #pulses per beat
#print('//Tempo (bpm)',mido.tempo2bpm(222000))  # from the exported CSV

REST = 0    # note number
tracks={1:[], 2:[], 3:[]}   # destination

# Find the range of notes we actually use
min_note = 255;
max_note = -1;

TRANSPOSE = 8  # take into account sample pitch

for i, track in enumerate(mid.tracks):
    if i<1 or i>3: continue

    print('//Track {}: {}'.format(i, track.name))
    abs_time = 0
    dest = tracks[i]    # what we are loading

    for msg in track:
        abs_time += msg.time

        if msg.is_meta: continue
        if msg.type!='note_on' and msg.type!='note_off': continue

        #print(msg.dict())

        note = msg.note + TRANSPOSE

        # Emit this message
        if msg.type == 'note_off':
            # One channel per instrument, so any note off is all note off.
            # Collapse adjacent note_off to rests.
            if len(dest)==0 or dest[-1][0]!=REST:
                dest.append([REST, abs_time/ppb, 0])
        else:   # note on
            dest.append([note, abs_time/ppb,
                            msg.velocity/127.0])
                # MIDI note (or -1 for rest), start time in beats, volume (0..1)
            if note > max_note: max_note = note
            if note < min_note: min_note = note
    # next msg
# next track

# Human-friendly debug check
print('//Notes from %d to %d'%(min_note,max_note))
if min_note<1: raise IndexError("I can't process songs that use note 0")

#print(tracks)
print('//#############################')
for trk in tracks:
    print('//Track %d'%trk)
    for msg in tracks[trk]:
        print('//',msg)
print('//#############################')

def format_song_item(item):
    # note, volume
    return 'vec2(%.20f, %.20f)'%(item[0], item[2])

def use_arr_idx(mid, item): return mid
def use_song_time(mid, item): return item[1]

BST_CALLBACK = format_song_item
BST_KEY = use_song_time

def print_fullsong_bst_helper(data, st, en, level=1):
    """ Print an if/then/else BST of msg.  st is the first index to print
    and en is the last index + 1.  This makes a _huge_ difference to speed
    compared to a linear lookup."""
    THLD = 4    # below this many data items, do it linear

    # Body
    if (en-st) > THLD:  # Recursive case
        mid = st + (en-st)//2   # integer divide
        print(id4(  ("if(key>=%.1f){"%BST_KEY(mid, data[mid])),
                    level))     # %.1f because float
        print_fullsong_bst_helper(data, mid, en, level+1)
        print(id4("}else{",level))
        print_fullsong_bst_helper(data, st, mid, level+1)
        print(id4("}",level))

    else:               # base case
        for idx in reversed(range(st, en)):
            print(id4("""if(key>=%.1f) return %s;"""%(BST_KEY(idx, data[idx]),
                BST_CALLBACK(data[idx])), level))
    #endif recurse else

# print_fullsong_bst_helper()

def print_fullsong(trackname, trackdata, in_var_name='beat'):
    # Helper header
    print(dedent("""\
        vec2 get_track_%s(float %s) {
            float key = %s;"""%(trackname, in_var_name, in_var_name)))

    # Helper body as a binary search tree
    print_fullsong_bst_helper(trackdata, 0, len(trackdata))

    # Helper footer
    print(dedent("""\
            return vec2(0.0);
        } //get_track_%s
        """%trackname))

# print_fullsong

tracknames={1:'square',2:'tri',3:'noise'}
for trackidx in range(1,4):
    print_fullsong(tracknames[trackidx], tracks[trackidx])

# Print the MIDI-note mapping from min_note to max_note.
# Map from note to (freq, period)

def MIDI_item(item): return 'vec2(%.20f,%.20f)'%item

BST_CALLBACK = MIDI_item
BST_KEY = use_arr_idx
print_fullsong('MIDINOTES',notedata, 'notenum')

# vi: set ts=4 sts=4 sw=4 et ai: #
